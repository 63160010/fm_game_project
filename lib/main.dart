import 'dart:js';

import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import 'package:fm_game_project/view/gamemode_list.dart';
import 'package:fm_game_project/view/models/ObjNumver.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(ChangeNotifierProvider(
    create: (context) => ObjectNumber(),
    child: const GameApp(),
  ));
}

class GameApp extends StatelessWidget {
  const GameApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'เกมคณิตคิดเร็ว',
        theme: ThemeData(
          primarySwatch: Colors.cyan,
        ),
        home: const ModeList(),
      ),
    );
  }
}//dd
