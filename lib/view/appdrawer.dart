import 'package:flutter/material.dart';
import 'package:fm_game_project/view/gamemode_list.dart';

class GameDrawer extends StatelessWidget {
  const GameDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          const SizedBox(
            height: 120.0,
            child: DrawerHeader(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: <Color>[Colors.black, Colors.blue]),
              ),
              child: Text(
                'Game Type',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 24,
                ),
              ),
            ),
          ),
          ListTile(
            leading: const Icon(Icons.calculate),
            title:
                const Text('เกมคณิตคิดเร็ว', style: TextStyle(fontSize: 18.0)),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const ModeList()),
              );
            },
          ),
        ],
      ),
    );
  }
}
