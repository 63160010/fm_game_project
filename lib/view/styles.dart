import 'package:flutter/material.dart';
import 'package:fm_game_project/view/data/stylemode.dart';
import 'package:fm_game_project/view/game_view.dart';

class StyleGame extends StatelessWidget {
  const StyleGame({
    Key? key,
  }) : super(key: key);

  static const _minimumCourseWidth = 300.0;

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;

    return LayoutBuilder(builder: (context, constraints) {
      final availableWidth = constraints.maxWidth;

      // How many StyleGame will fit, horizontally?
      final StyleGamePerRow = (availableWidth / _minimumCourseWidth).floor();
      final numberOfRows =
          (StyleMode.shortList.length / StyleGamePerRow).ceil();

      final customGrid = <Widget>[];

      // Loop over all rows in the custom grid
      for (var row = 0; row < numberOfRows; row++) {
        final StyleGameInRow = <Widget>[];

        // Loop over all StyleGame in the current row
        for (var currentRowIndex = 0;
            currentRowIndex < StyleGamePerRow;
            currentRowIndex++) {
          final courseGlobalIndex = row * StyleGamePerRow + currentRowIndex;

          // If the last row is not full, break the loop
          if (courseGlobalIndex >= StyleMode.shortList.length) {
            // The row is not full, add a spacer
            StyleGameInRow.add(const Spacer(
              flex: 2,
            ));
            break;
          }

          // If there are more StyleGame to add, add them to the current row
          final currentCourse = StyleMode.shortList[courseGlobalIndex];
          StyleGameInRow.add(
            Expanded(
              child: ConstrainedBox(
                constraints: const BoxConstraints(
                  // minWidth: double.infinity,
                  maxHeight: 420,
                ),
                child: GameView(mode: currentCourse),
              ),
            ),
          );
        }

        // Add the current row to the custom grid
        customGrid.add(Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: StyleGameInRow,
        ));
      }
      return ListView(
        children: customGrid,
      );
    });
  }
}
