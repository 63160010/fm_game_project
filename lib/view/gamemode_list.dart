import 'package:flutter/material.dart';
import 'package:fm_game_project/view/appdrawer.dart';
import 'package:fm_game_project/view/game_layout.dart';
import 'package:fm_game_project/view/game_menu.dart';
import 'package:fm_game_project/view/styles.dart';

class ModeList extends StatelessWidget {
  const ModeList({super.key});

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    final smallerDevice = width < Gamelayout.widthFirstBreakpoint;

    const gameStyle = StyleGame();
    const gamelist = Padding(
      padding: EdgeInsets.all(12.0),
      child: gameStyle,
    );

    return Scaffold(
      appBar: AppBar(
        iconTheme: const IconThemeData(color: Colors.white),
        title: Image.network(
          "lib/view/image/math-sign.png",
          fit: BoxFit.contain,
          height: 150,
        ),
        toolbarHeight: 88,
        flexibleSpace: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: <Color>[Colors.black, Colors.blue]),
          ),
        ),
      ),
      drawer: const GameDrawer(),
      body: Column(
        children: [
          Expanded(
            child: Container(
              width: width,
              height: height,
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [Colors.blue, Colors.black],
                ),
              ),
              child: smallerDevice
                  ? gamelist
                  : Row(
                      children: const [
                        Expanded(
                          flex: 1,
                          child: GameMenu(),
                        ),
                        Expanded(flex: 6, child: gamelist),
                      ],
                    ),
            ),
          ),
        ],
      ),
    );
  }
}
