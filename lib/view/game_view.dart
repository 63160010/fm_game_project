import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fm_game_project/view/data/stylemode.dart';
import 'package:fm_game_project/view/gamemode_list.dart';
import 'package:fm_game_project/view/Page/dividePage.dart';
import 'package:fm_game_project/view/Page/minusPage.dart';
import 'package:fm_game_project/view/Page/multiplyPage.dart';
import 'package:fm_game_project/view/Page/plusPage.dart';
import 'package:provider/provider.dart';

import 'models/ObjNumver.dart';

class GameView extends StatelessWidget {
  final StyleMode mode;

  const GameView({
    Key? key,
    required this.mode,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ObjectNumber objects = Provider.of<ObjectNumber>(context);
    // หน้าจอเกมแต่ละหน้า
    List pagegame = [
      // หน้าจอเกมบวก เช่น const plusPage(),
      // หน้าจอเกมลบ เช่น const minusPage(),
      // หน้าจอเกมคูณ เช่น const multiplyPage(),
      // หน้าจอเกมหาร  เช่น const dividePage(),
      const PlusPage(),
      const MinusPage(),
      const MultiplyPage(),
      const DividePage(),
    ];

    return InkWell(
      splashColor: Colors.transparent,
      onTap: () {
        if (pagegame[mode.page] != null) {
          if (mode.page == 0) {
            objects.goToNextQuestions1();
          } else if (mode.page == 1) {
            objects.goToNextQuestions2();
          } else if (mode.page == 2) {
            objects.goToNextQuestions();
          }
          objects.startTime();
          Navigator.push(
            context,
            CupertinoModalPopupRoute(
              builder: (context) => pagegame[mode.page],
            ),
          );
        }
      },
      child: Card(
        elevation: 1,
        shadowColor: Colors.black45.withAlpha(125),
        semanticContainer: true,
        shape: const BeveledRectangleBorder(),
        child: Container(
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: NetworkImage("lib/view/image/unvise.jpg"),
              fit: BoxFit.cover,
              alignment: Alignment.topCenter,
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 24, horizontal: 24),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      height: 200,
                      color: mode.color,
                      child: Center(
                        child: Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 24, horizontal: 24),
                            child: Image.network(mode.images)),
                      ),
                    ),
                    const SizedBox(height: 24),
                    Text(
                      mode.title,
                      style: const TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    ),
                    const SizedBox(height: 8),
                    Text(
                      mode.description,
                      style: const TextStyle(fontSize: 12, color: Colors.white),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
