import 'package:fm_game_project/view/game_layout.dart';
import 'package:flutter/material.dart';
import 'package:fm_game_project/view/models/ObjNumver.dart';
import 'package:provider/provider.dart';

// number A, number B, questionAll, level, time, score
int numberA = 0;
int numberB = 0;
int question = 0;
//การกำหนวดค่าคำตอบข้อแรก
int question1 = 0;
int question2 = 0;
int question3 = 0;
int level = 0;
int time = 5;
int score = 0;

class PlusPage extends StatefulWidget {
  const PlusPage({Key? key}) : super(key: key);
  @override
  State<PlusPage> createState() => _PlusPageState();
}

class _PlusPageState extends State<PlusPage> {
  // create time
//   Timer? timer;
//
//   @override
//   void initState() {
//     // TODO: implement initState
//     super.initState();
//     WidgetsBinding.instance.addPostFrameCallback((_) {
//       startTime();
//       setState(() {
//         goToNextQuestion();
//       });
//     });
//   }
//
//   //เงื่อนไขการนับถอยหลัง 5 วินาที
//   void startTime() {
//     timer = Timer.periodic(const Duration(seconds: 1), (_) {
//       setState(() => time--);
//       if (time <= 0) {
//         stopTime();
//         showDialog(
//             context: context,
//             builder: (context) {
//               return ResultMessage(
//                 message: 'หมดเวลา!',
//                 onTap: goBackToStart,
//                 icon: Icons.not_started,
//               );
//             });
//       }
//     });
//   }
//
// //หมดเวลา
//   void stopTime() {
//     timer?.cancel();
//   }
//
// // create random numbers
//   //ตัวแปรการสุ่ม
//   var rng = Random();
//
//   // user answer
//   int userAnswer = 0;
//
  // check if user is correct or not
  //เงื่อนไขการเช็คคำตอบ ถ้าเลขที่สุ่มทั้งสองหลักเท่ากับ ช้อยข้อไหนให้ แสดงข้อความถูกต้องแล้วให้คลิกไปข้อถัดไป
  // ถ้าผิด แสดงข้อความลองอีกครั้ง เมื่อทำการกดลองอีกครั้ง จะทำการเล่นต่อไปเรื่อยๆ จนกว่าเวลาจะหมด
  // ถ้าเวลาหมดแล้วทำการรีเซ็ตคะแนนเวลาทั้งหมดแล้วเล่นข้อที่ผิดซ้ำอีกครั้ง
//   void checkResult1() {
//     if (numberA + numberB == question1) {
//       stopTime();
//       showDialog(
//           context: context,
//           builder: (context) {
//             return ResultMessage(
//               message: 'ถูกต้อง!',
//               onTap: goToNextQuestion1,
//               icon: Icons.arrow_forward,
//             );
//           });
//       time = 5;
//     } else {
//       stopTime();
//       showDialog(
//           context: context,
//           builder: (context) {
//             return ResultMessage(
//               message: 'ลองอีกครั้ง',
//               onTap: goToNextQuestion1,
//               icon: Icons.rotate_left,
//             );
//           });
//     }
//   }
//
//   void checkResult2() {
//     if (numberA + numberB == question2) {
//       stopTime();
//       showDialog(
//           context: context,
//           builder: (context) {
//             return ResultMessage(
//               message: 'ถูกต้อง!',
//               onTap: goToNextQuestion1,
//               icon: Icons.arrow_forward,
//             );
//           });
//       time = 5;
//     } else {
//       stopTime();
//       showDialog(
//           context: context,
//           builder: (context) {
//             return ResultMessage(
//               message: 'ลองอีกครั้ง',
//               onTap: goToNextQuestion1,
//               icon: Icons.rotate_left,
//             );
//           });
//     }
//   }
//
//   void checkResult3() {
//     if (numberA + numberB == question3) {
//       stopTime();
//       showDialog(
//           context: context,
//           builder: (context) {
//             return ResultMessage(
//               message: 'ถูกต้อง!',
//               onTap: goToNextQuestion1,
//               icon: Icons.arrow_forward,
//             );
//           });
//       time = 5;
//     } else {
//       stopTime();
//       showDialog(
//           context: context,
//           builder: (context) {
//             return ResultMessage(
//               message: 'ลองอีกครั้ง',
//               onTap: goToNextQuestion1,
//               icon: Icons.rotate_left,
//             );
//           });
//     }
//   }
//
//   void goToNextQuestion1() {
//     // create a new question
//     Navigator.of(context).pop();
//     setState(() {
//       userAnswer = 0;
//     });
//
//     startTime();
//
//     numberA = rng.nextInt(11);
//     numberB = rng.nextInt(11);
//     question = rng.nextInt(3) + 1;
//
//     //check if QUESTION
//     if (question == 1) {
//       question1 = numberA + numberB;
//       question2 = rng.nextInt(20);
//       question3 = rng.nextInt(20);
//
//       // Make sure the number is not duplicate.
//       //เงื่อนไขการเช็คว่าคำตอบช้อยที่ออกมาจำไม่มีช้อยที่ซ้ำกัน ถ้าเหมือนจะทำการสุ่มช้อยตัวเลือกใหม่
//       if (question2 == question1 || question2 == question3) {
//         question2 = rng.nextInt(20);
//       } else if (question3 == question1) {
//         question3 = rng.nextInt(20);
//       }
//     }
//
//     if (question == 2) {
//       question2 = numberA + numberB;
//       question1 = rng.nextInt(20);
//       question3 = rng.nextInt(20);
//
//       if (question1 == question2 || question1 == question3) {
//         question1 = rng.nextInt(20);
//       } else if (question3 == question2) {
//         question3 = rng.nextInt(20);
//       }
//     }
//
//     if (question == 3) {
//       question3 = numberA + numberB;
//       question1 = rng.nextInt(20);
//       question2 = rng.nextInt(20);
//
//       if (question1 == question3 || question1 == question2) {
//         question1 = rng.nextInt(20);
//       } else if (question2 == question3) {
//         question2 = rng.nextInt(20);
//       }
//     }
//     //คะแนนการตอบถูก
//     score += 2;
//     //จำข้อที่เล่นได้ในแต่ละรอบ
//     level++;
//   }
//
//   // GO BACK TO QUESTION
//   //ลองอีกครั้งปิดข้อความแจ้งเตือนแล้วเริ่มเวลานับใหม่ต่อจากข้อเดิม
//   void goBackToQuestion() {
//     // dismiss alert dialog
//     Navigator.of(context).pop();
//
//     startTime();
//   }
//
// //ออกจากเกมจะทำการเริ่มใหม่ตั้งแต่ต้น
//   // GO BACK TO START GAME
//   void goBackToStart() {
//     // dismiss alert dialog
//     Navigator.of(context).pop();
//
//     setState(() {
//       score = 0;
//       level = 0;
//       time = 5;
//     });
//     startTime();
//   }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    final smallerDevice = width < Gamelayout.widthFirstBreakpoint;

    final ObjectNumber objects = Provider.of<ObjectNumber>(context);
    return Scaffold(
      body: Stack(
        children: [
          // background
          Container(
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: NetworkImage("lib/view/image/pngtree.jpg"),
                fit: BoxFit.cover,
              ),
            ),
          ),

          Column(
            children: [
              // score and time
              SizedBox(
                height: 150,
                child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Icon(
                        Icons.timer,
                        color: Colors.white,
                        size: 50,
                      ),
                      Text(
                        ': ${objects.time}',
                        style: blackTextStyle,
                      ),
                      const SizedBox(width: 20),
                      Text(
                        'LEVEL: ${objects.level}',
                        style: blackTextStyle,
                      ),
                      const SizedBox(width: 20),
                      const Icon(
                        Icons.star,
                        color: Colors.yellow,
                        size: 50,
                      ),
                      Text(
                        ': ${objects.score}',
                        style: blackTextStyle,
                      ),
                    ],
                  ),
                ),
              ),

              // question
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    decoration: const BoxDecoration(
                      image: DecorationImage(
                        image: NetworkImage("lib/view/image/background.jpg"),
                        fit: BoxFit.fill,
                      ),
                    ),
                    child: Center(
                      child: Text(
                        '${objects.numberA} + ${objects.numberB} = ?',
                        style: whiteTextStyle,
                      ),
                    ),
                  ),
                ),
              ),

              // number radom ans 3 ch
              Expanded(
                flex: 2,
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    children: [
                      SizedBox(
                        width: 200,
                        child: Container(
                          // width: 50,
                          // height: 50,
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                Colors.pinkAccent.shade100,
                                Colors.pinkAccent.shade400
                              ],
                            ),
                          ),
                          child: TextButton(
                            onPressed: () => {
                              if (objects.question1 ==
                                  objects.numberA + objects.numberB)
                                {
                                  objects.stopTime(),
                                  showDialog(
                                      context: context,
                                      builder: (context) {
                                        return ResultMessage(
                                          message: 'ถูกต้อง!',
                                          onTap: () => {
                                            objects.goToNextQuestions1(),
                                            objects.scoreNumber(objects.time),
                                            objects.levelNumber(),
                                            objects.time = 10,
                                            objects.startTime(),
                                            Navigator.of(context).pop(),
                                          },
                                          icon: Icons.arrow_forward,
                                        );
                                      }),
                                  // dialogPass(),
                                  // checkResult1(),
                                }
                              else
                                {
                                  // checkResult1(),
                                  // dialogFalse(),
                                  objects.stopTime(),
                                  showDialog(
                                      context: context,
                                      builder: (context) {
                                        return ResultMessage(
                                          message: 'ลองอีกครั้ง',
                                          onTap: () => {
                                            objects.goToNextQuestions1(),
                                            // objects.time = 5,
                                            objects.levelNumber(),
                                            objects.startTime(),
                                            Navigator.of(context).pop(),
                                          },
                                          icon: Icons.rotate_left,
                                        );
                                      }),
                                },
                              print (objects.numberA),
                              print (objects.numberB),
                              print (objects.numberA + objects.numberB),
                            },
                            style: TextButton.styleFrom(
                              side: const BorderSide(
                                  width: 3.0, color: Colors.white),
                              // backgroundColor: Colors.amber
                            ),
                            child: Text(objects.question1.toString(),
                                style: whiteTextStyleQ),
                          ),
                        ),
                      ),
                      const SizedBox(height: 20),
                      SizedBox(
                        width: 200,
                        child: Container(
                          // width: 50,
                          // height: 50,
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                Colors.pinkAccent.shade100,
                                Colors.pinkAccent.shade400
                              ],
                            ),
                          ),
                          child: TextButton(
                            onPressed: () => {
                              if (objects.question2 ==
                                  objects.numberA + objects.numberB)
                                {
                                  objects.stopTime(),
                                  showDialog(
                                      context: context,
                                      builder: (context) {
                                        return ResultMessage(
                                          message: 'ถูกต้อง!',
                                          onTap: () => {
                                            objects.goToNextQuestions1(),
                                            objects.scoreNumber(objects.time),
                                            objects.levelNumber(),
                                            objects.time = 10,
                                            objects.startTime(),
                                            Navigator.of(context).pop(),
                                          },
                                          icon: Icons.arrow_forward,
                                        );
                                      }),
                                  // dialogPass(),
                                  // checkResult2(),
                                }
                              else
                                {
                                  // checkResult2(),
                                  // dialogFalse(),
                                  objects.stopTime(),
                                  showDialog(
                                      context: context,
                                      builder: (context) {
                                        return ResultMessage(
                                          message: 'ลองอีกครั้ง',
                                          onTap: () => {
                                            objects.goToNextQuestions1(),
                                            // objects.time = 10,
                                            objects.levelNumber(),
                                            objects.startTime(),
                                            Navigator.of(context).pop(),
                                          },
                                          icon: Icons.rotate_left,
                                        );
                                      }),
                                }
                            },
                            style: TextButton.styleFrom(
                              side: const BorderSide(
                                  width: 3.0, color: Colors.white),
                              // backgroundColor: Colors.amber
                            ),
                            child: Text(objects.question2.toString(),
                                style: whiteTextStyleQ),
                          ),
                        ),
                      ),
                      const SizedBox(height: 20),
                      SizedBox(
                        width: 200,
                        child: Container(
                          // width: 50,
                          // height: 50,
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                Colors.pinkAccent.shade100,
                                Colors.pinkAccent.shade400
                              ],
                            ),
                          ),
                          child: TextButton(
                            onPressed: () => {
                              if (objects.question3 ==
                                  objects.numberA + objects.numberB)
                                {
                                  objects.stopTime(),
                                  showDialog(
                                      context: context,
                                      builder: (context) {
                                        return ResultMessage(
                                          message: 'ถูกต้อง!',
                                          onTap: () => {
                                            objects.goToNextQuestions1(),
                                            objects.scoreNumber(objects.time),
                                            objects.levelNumber(),
                                            objects.time = 10,
                                            objects.startTime(),
                                            Navigator.of(context).pop(),
                                          },
                                          icon: Icons.arrow_forward,
                                        );
                                      }),
                                  // dialogPass(),
                                  // checkResult3(),
                                }
                              else
                                {
                                  // checkResult3(),
                                  // dialogFalse(),
                                  objects.stopTime(),
                                  showDialog(
                                      context: context,
                                      builder: (context) {
                                        return ResultMessage(
                                          message: 'ลองอีกครั้ง',
                                          onTap: () => {
                                            objects.goToNextQuestions1(),
                                            // objects.time = 10,
                                            objects.levelNumber(),
                                            objects.startTime(),
                                            Navigator.of(context).pop(),
                                          },
                                          icon: Icons.rotate_left,
                                        );
                                      }),
                                }
                            },
                            style: TextButton.styleFrom(
                              side: const BorderSide(
                                  width: 3.0, color: Colors.white),
                              // backgroundColor: Colors.amber
                            ),
                            child: Text(objects.question3.toString(),
                                style: whiteTextStyleQ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),

              FloatingActionButton(
                onPressed: () {
                  objects.stopTime();

                  setState(() {
                    objects.time = 10;
                    objects.level = 0;
                    objects.score = 0;
                  });
                  // Navigate back.
                  Navigator.of(context).pop();
                },
                mini: true,
                child: const Icon(Icons.close),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

var blackTextStyle = const TextStyle(
  fontWeight: FontWeight.bold,
  fontSize: 32,
  color: Colors.black,
);

var whiteTextStyle = const TextStyle(
  fontWeight: FontWeight.bold,
  fontSize: 64,
  color: Colors.white,
);

var whiteTextStyleQ = const TextStyle(
  fontWeight: FontWeight.bold,
  fontSize: 50,
  color: Colors.white,
);

var whiteTextStyle1 = const TextStyle(
  fontWeight: FontWeight.bold,
  fontSize: 32,
  color: Colors.white,
);
// void goToNextQuestion() {
//
//   // create a new question
//   var rng = Random();
//   numberA = rng.nextInt(11);
//   numberB = rng.nextInt(11);
//   question = rng.nextInt(3) + 1;
//
//   //check if QUESTION
//   if (question == 1) {
//     question1 = numberA + numberB;
//     question2 = rng.nextInt(20);
//     question3 = rng.nextInt(20);
//
//     // Make sure the number is not duplicate.
//     //เงื่อนไขการเช็คว่าคำตอบช้อยที่ออกมาจำไม่มีช้อยที่ซ้ำกัน ถ้าเหมือนจะทำการสุ่มช้อยตัวเลือกใหม่
//     if (question2 == question1 || question2 == question3) {
//       question2 = rng.nextInt(20);
//     } else if (question3 == question1) {
//       question3 = rng.nextInt(20);
//     }
//   }
//
//   if (question == 2) {
//     question2 = numberA + numberB;
//     question1 = rng.nextInt(20);
//     question3 = rng.nextInt(20);
//
//     if (question1 == question2 || question1 == question3) {
//       question1 = rng.nextInt(20);
//     } else if (question3 == question2) {
//       question3 = rng.nextInt(20);
//     }
//   }
//
//   if (question == 3) {
//     question3 = numberA + numberB;
//     question1 = rng.nextInt(20);
//     question2 = rng.nextInt(20);
//
//     if (question1 == question3 || question1 == question2) {
//       question1 = rng.nextInt(20);
//     } else if (question2 == question3) {
//       question2 = rng.nextInt(20);
//     }
//   }
// }

class ResultMessage extends StatelessWidget {
  final String message;
  final VoidCallback onTap;
  final icon;

  const ResultMessage({
    Key? key,
    required this.message,
    required this.onTap,
    required this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: Colors.green,
      content: SizedBox(
        height: 200,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            // the result
            Text(
              message,
              style: whiteTextStyle1,
            ),

            // button to go to next question
            GestureDetector(
              onTap: onTap,
              child: Container(
                padding: const EdgeInsets.all(4),
                decoration: BoxDecoration(
                  color: Colors.red[400],
                  borderRadius: BorderRadius.circular(8),
                ),
                child: Icon(
                  icon,
                  color: Colors.white,
                ),
              ),
            ),
          ],
        ),
      ),
    );

  } //
}
