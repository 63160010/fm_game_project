import 'package:fm_game_project/view/game_layout.dart';
import 'package:flutter/material.dart';
import 'package:fm_game_project/view/models/ObjNumver.dart';
import 'package:provider/provider.dart';

int numberB = 0;
int numberA = 0;
int question = 0;
int question1 = 0;
int question2 = 0;
int question3 = 0;
int level = 0;
int time = 5;
int score = 0;

class DividePage extends StatefulWidget {
  const DividePage({Key? key}) : super(key: key);
  @override
  State<DividePage> createState() => _DividePageState();
}

class _DividePageState extends State<DividePage> {
  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    final smallerDevice = width < Gamelayout.widthFirstBreakpoint;
    final ObjectNumber objects = Provider.of<ObjectNumber>(context);
    return Scaffold(
      body: Stack(
          children: [
      // background
      Container(
      decoration: const BoxDecoration(
      image: DecorationImage(
          image: NetworkImage("lib/view/image/game-background2.jpg"),
      fit: BoxFit.cover,
    ),
    ),
    ),

    Column(
    children: [
    // score and time
    SizedBox(
    height: 150,
    child: Center(
    child: Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
    const Icon(
    Icons.timer,
    color: Colors.grey,
    size: 50,
    ),
    Text(
    ': ${objects.time}',
    style: blackTextStyle,
    ),
    const SizedBox(width: 20),
    Text(
      'LEVEL: ${objects.level}',
    style: blackTextStyle,
    ),
    const SizedBox(width: 20),
    const Icon(
    Icons.star,
    color: Colors.yellow,
    size: 50,
    ),
    Text(
      ': ${objects.score}',
    style: blackTextStyle,
    ),
    ],
    ),
    ),
    ),

    // question
    Expanded(
    child: Padding(
    padding: const EdgeInsets.all(8.0),
    child: Container(
    decoration: const BoxDecoration(
    image: DecorationImage(
    image: NetworkImage("lib/view/image/background.jpg"),
    fit: BoxFit.fill,
    ),
    ),
    child: Center(
    child: Text(
    '${objects.numberA} ÷ ${objects.numberB}  = ?',
    style: whiteTextStyle,
    ),
    ),
    ),
    ),
    ),
    Expanded(
    flex: 2,
    child: Padding(
    padding: const EdgeInsets.all(16.0),
    child: Column(
    children: [
    SizedBox(
    width: 200,
    child: Container(
    // width: 50,
    // height: 50,
    decoration: BoxDecoration(
    gradient: LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [
    Colors.redAccent,
    Colors.red.shade900
    ],
    ),
    ),
    child: TextButton(
    onPressed: () => {
    if (  objects.question1==objects.numberA /objects.numberB
    )
    {
    objects.stopTime(),
    showDialog(
    context: context,
    builder: (context) {
    return ResultMessage(
    message: 'ถูกต้อง!',
    onTap: ()=>{
    objects.goToNextQuestions2(),
    objects.scoreNumber(objects.time),
    objects.levelNumber(),
    objects.time = 10,
    objects.startTime(),
    Navigator.of(context).pop(),
    },
    icon: Icons.arrow_forward,
    );
    }),
    }
    else
    {
    objects.stopTime(),
    showDialog(
    context: context,
    builder: (context) {
    return ResultMessage(
    message: 'ลองอีกครั้ง',
    onTap: () =>{
    objects.goToNextQuestions3(),
    objects.levelNumber(),
    objects.startTime(),
    Navigator.of(context).pop(),
    },
    icon: Icons.rotate_left,
    );
    }),
    },
    },
    style: TextButton.styleFrom(
    side: const BorderSide(
    width: 3.0, color: Colors.white),
    // backgroundColor: Colors.amber
    ),
    child: Text(objects.question1.toString(),
    style: whiteTextStyleQ),
    ),
    ),
    ),
    const SizedBox(height: 20),
    SizedBox(
    width: 200,
    child: Container(
    // width: 50,
    // height: 50,
    decoration: BoxDecoration(
    gradient: LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [
    Colors.redAccent,
    Colors.red.shade900
    ],
    ),
    ),
    child: TextButton(
    onPressed: () => {
      if (  objects.question2==objects.numberA /objects.numberB
      )
    {

    objects.stopTime(),
    showDialog(
    context: context,
    builder: (context) {
    return ResultMessage(
    message: 'ถูกต้อง!',
    onTap:  () => {
    objects.goToNextQuestions3(),
    objects.scoreNumber(objects.time),
    objects.levelNumber(),
    objects.time = 10,
    objects.startTime(),
    Navigator.of(context).pop(),
    },
    icon: Icons.arrow_forward,
    );
    }),

    }
    else
    {


    objects.stopTime(),
    showDialog(
    context: context,
    builder: (context) {
    return ResultMessage(
    message: 'ลองอีกครั้ง',
    onTap: () => {
    objects.goToNextQuestions2(),
    objects.levelNumber(),
    objects.startTime(),
    Navigator.of(context).pop(),
    },
    icon: Icons.rotate_left,
    );
    }),
    },
    },
    style: TextButton.styleFrom(
    side: const BorderSide(
    width: 3.0, color: Colors.white),
    // backgroundColor: Colors.amber
    ),
    child: Text(objects.question2.toString(),
    style: whiteTextStyleQ),
    ),
    ),
    ),
    const SizedBox(height: 20),
    SizedBox(
    width: 200,
    child: Container(
    // width: 50,
    // height: 50,
    decoration: BoxDecoration(
    gradient: LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [Colors.redAccent,
    Colors.red.shade900
    ],
    ),
    ),
    child: TextButton(
    onPressed: () => {
      if (  objects.question3==objects.numberA /objects.numberB
      ) {
    objects.stopTime(),
    showDialog(
    context: context,
    builder: (context) {
    return ResultMessage(
    message: 'ถูกต้อง!',
    onTap: () => {
    objects.goToNextQuestions3(),
    objects.scoreNumber(objects.time),
    objects.levelNumber(),
    objects.time = 10,
    objects.startTime(),
    Navigator.of(context).pop(),
    }, icon: Icons.arrow_forward,
    );
    }),
  }
  else
  {
    objects.stopTime(),
    showDialog(
        context: context,
        builder: (context) {
          return ResultMessage(
            message: 'ลองอีกครั้ง',
            onTap: () => {
              objects.goToNextQuestions3(),
              // objects.time = 5,
              objects.levelNumber(),
              objects.startTime(),
              Navigator.of(context).pop(),
            },
            icon: Icons.rotate_left,
          );
        }),
  },
  },
    style: TextButton.styleFrom(
    side: const BorderSide(
    width: 3.0, color: Colors.white),
    // backgroundColor: Colors.amber
    ),
    child: Text(objects.question3.toString(),
    style: whiteTextStyleQ),

    ),
    ),
    ),
    ],
    ),
    ),
    ),

    FloatingActionButton(
    onPressed: () {
      objects.stopTime();
      setState(() {
        objects.time = 10;
        objects.level = 0;
        objects.score = 0;
      });
    // Navigate back.
    Navigator.of(context).pop();
    },
    mini: true,
    child: const Icon(Icons.close),
    ),
    ],
    ),
    ],
    ),
    );
  }
}

var blackTextStyle = const TextStyle(
  fontWeight: FontWeight.bold,
  fontSize: 32,
  color: Colors.white,
);

var whiteTextStyle = const TextStyle(
  fontWeight: FontWeight.bold,
  fontSize: 64,
  color: Colors.white,
);

var whiteTextStyleQ = const TextStyle(
  fontWeight: FontWeight.bold,
  fontSize: 50,
  color: Colors.white,
);

var whiteTextStyle1 = const TextStyle(
  fontWeight: FontWeight.bold,
  fontSize: 32,
  color: Colors.white,
);

class ResultMessage extends StatelessWidget {
  final String message;
  final VoidCallback onTap;
  final icon;

  const ResultMessage({
    Key? key,
    required this.message,
    required this.onTap,
    required this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: Colors.orange,
      content: Container(
        height: 200,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            // the result
            Text(
              message,
              style: whiteTextStyle1,
            ),

            // button to go to next question
            GestureDetector(
              onTap: onTap,
              child: Container(
                padding: const EdgeInsets.all(4),
                decoration: BoxDecoration(
                  color: Colors.orange[400],
                  borderRadius: BorderRadius.circular(8),
                ),
                child: Icon(
                  icon,
                  color: Colors.white,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}//
