import 'package:flutter/material.dart';
import 'package:fm_game_project/view/gamemode_list.dart';

class GameMenu extends StatelessWidget {
  const GameMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: SafeArea(
        right: false,
        child: Column(
          children: <Widget>[
            IconButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const ModeList()),
                );
              },
              icon: const Icon(Icons.calculate),
            ),
          ],
        ),
      ),
    );
  }
}
