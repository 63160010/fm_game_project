import 'package:flutter/material.dart';

class StyleMode {
  final String images;
  final String title;
  final String description;
  final int page;
  final double completed;
  final Color color;
  final List<String> lessons;

  const StyleMode({
    required this.images,
    required this.title,
    required this.description,
    required this.page,
    required this.completed,
    required this.color,
    this.lessons = const [],
  });

  static List<String> _copyLessons(int quantity, List<String> lessons) {
    var returnLessons = lessons;
    for (var i = 0; i < quantity; i++) {
      returnLessons += lessons;
    }
    return returnLessons;
  }

  static final shortList = [
    StyleMode(
        images: 'lib/view/image/maths-sum.png',
        title: 'เกมบวก',
        description:
            '''คือกระบวนการทางคณิตศาสตร์ โดยการรวมสิ่งของเข้าด้วยกันเครื่องหมายบวก (+) ถูกใช้แทนความหมายของการบวกจำนวนหลายจำนวน3+3=6''',
        page: 0,
        completed: 0.3,
        lessons: ['Install Dart', 'Run Dart', 'Profit'],
        color: Colors.pink[200]!),
    StyleMode(
        images: 'lib/view/image/maths-del.png',
        title: 'เกมลบ',
        description:
            '''คือการนำจำนวนหนึ่งหักออกจากอีกจำนวนหนึ่ง หรือเป็นการเปรียบเทียบจำนวนสองจำนวน ซึ่งจำนวนที่เหลือหรือจำนวนที่เป็นผลต่างของสองจำนวนนี้เรียกว่า “ผลลบ” และใช้เครื่องหมาย ลบ (-) เป็นการแสดงการลบ 5-3=2''',
        page: 1,
        completed: 0.7,
        lessons: [
          'Install Flutter',
          'Open Android Studio',
          'Write Code',
          'Debug on Simulator',
          ..._copyLessons(10, ['Write More Code', 'More Debug on Simulator']),
        ],
        color: Colors.yellow[200]!),
    StyleMode(
        images: 'lib/view/image/maths-mul.png',
        title: 'เกมคูณ',
        description:
            '''คือการดำเนินการทางคณิตศาสตร์อย่างหนึ่ง ทำให้เกิดการเพิ่มหรือลดจำนวนจำนวนหนึ่งเป็นอัตรา การคูณเป็นหนึ่งในสี่ของการดำเนินการพื้นฐานของเลขคณิตมูลฐาน (การดำเนินการอย่างอื่นได้แก่ การบวก การลบ และการหาร)''',
        page: 2,
        completed: 0.6,
        color: Colors.cyan[200]!),
    StyleMode(
        images: 'lib/view/image/maths-div.png',
        title: 'เกมหาร',
        description:
            '''คือการทำให้ลดลงทีละเท่าๆกัน หรือการลบออกด้วยจำนวนเท่าๆกันหลายครั้ง จนผลลบเป็น 0''',
        page: 3,
        completed: 0.2,
        color: Colors.deepPurple[200]!),
  ];
}
