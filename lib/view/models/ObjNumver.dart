import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';

import 'package:flutter/widgets.dart';

import '../Page/multiplyPage.dart';

class ObjectNumber with ChangeNotifier {
  var randomNumber = Random();

  int score = 0;
  int level = 0;
  int numberA = 0;
  int numberB = 0;
  int question = 0;
  int question1 = 0;
  int question2 = 0;
  int question3 = 0;
  Timer? timer;
  int time = 10;

  void scoreNumber(times) {
    if (times >= 8) {
      score += 3;
    } else if (times >= 4 && time <= 7) {
      score += 2;
    } else {
      score += 1;
    }
    notifyListeners();
  }

  void levelNumber() {
    level++;
    notifyListeners();
  }

  void startTime() {
    timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      if (time > 0) {
        time--;
        notifyListeners();
      } else {
        timer.cancel();
      }
    });
  }

  void stopTime() {
    timer?.cancel();
  }

  void goToNextQuestions() {
    numberA = randomNumber.nextInt(6);
    numberB = randomNumber.nextInt(6);
    question = randomNumber.nextInt(3) + 1;

    //check if QUESTION
    if (question == 1) {
      question1 = numberA * numberB;
      question2 = randomNumber.nextInt(26);
      question3 = randomNumber.nextInt(26);

      // Make sure the number is not duplicate.
      if (question2 == question1 || question2 == question3) {
        question2 = randomNumber.nextInt(26);
      } else if (question3 == question1) {
        question3 = randomNumber.nextInt(26);
      }
    }

    if (question == 2) {
      question2 = numberA * numberB;
      question1 = randomNumber.nextInt(26);
      question3 = randomNumber.nextInt(26);

      if (question1 == question2 || question1 == question3) {
        question1 = randomNumber.nextInt(26);
      } else if (question3 == question2) {
        question3 = randomNumber.nextInt(26);
      }
    }

    if (question == 3) {
      question3 = numberA * numberB;
      question1 = randomNumber.nextInt(26);
      question2 = randomNumber.nextInt(26);

      if (question1 == question3 || question1 == question2) {
        question1 = randomNumber.nextInt(26);
      } else if (question2 == question3) {
        question2 = randomNumber.nextInt(26);
      }
    }
  }
  void goToNextQuestions1() {
    numberA = randomNumber.nextInt(11);
    numberB = randomNumber.nextInt(11);
    question = randomNumber.nextInt(3)+1 ;

    if (question == 1) {
      question1 = numberA + numberB;
      question2 = randomNumber.nextInt(20);
      question3 = randomNumber.nextInt(20);
      if (question2 == question1 || question2 == question3) {
        question2 = randomNumber.nextInt(20);
      } else if (question3 == question1) {
        question3 = randomNumber.nextInt(20);
      }
    }

    if (question == 2) {
      question2 = numberA + numberB;
      question1 = randomNumber.nextInt(20);
      question3 = randomNumber.nextInt(20);
      if (question1 == question2 || question1 == question3) {
        question1 = randomNumber.nextInt(20);
      } else if (question3 == question2) {
        question3 = randomNumber.nextInt(20);
      }
    }

    if (question == 3) {
      question3 = numberA + numberB;
      question1 = randomNumber.nextInt(20);
      question2 = randomNumber.nextInt(20);
      if (question1 == question3 || question1 == question2) {
        question1 = randomNumber.nextInt(20);
      } else if (question2 == question3) {
        question2 = randomNumber.nextInt(20);
      }
    }
  }
  void goToNextQuestions2() {
    numberA = randomNumber.nextInt(11);
    numberB = randomNumber.nextInt(11);
    question = randomNumber.nextInt(3) + 1;


    if (question == 1) {
      question1 = numberA - numberB;
      question2 = randomNumber.nextInt(20);
      question3 = randomNumber.nextInt(20);
      if (question2 == question1 || question2 == question3) {
        question2 = randomNumber.nextInt(20);
      } else if (question3 == question1) {
        question3 = randomNumber.nextInt(20);
      }
    }

    if (question == 2) {
      question2 = numberA - numberB;
      question1 = randomNumber.nextInt(20);
      question3 = randomNumber.nextInt(20);
      if (question1 == question2 || question1 == question3) {
        question1 = randomNumber.nextInt(20);
      } else if (question3 == question2) {
        question3 = randomNumber.nextInt(20);
      }
    }

    if (question == 3) {
      question3 = numberA - numberB;
      question1 = randomNumber.nextInt(20);
      question2 = randomNumber.nextInt(20);
      if (question1 == question3 || question1 == question2) {
        question1 = randomNumber.nextInt(20);
      } else if (question2 == question3) {
        question2 = randomNumber.nextInt(20);
      }
    }
  }
  void goToNextQuestions3() {

    numberB = randomNumber.nextInt(6)+1;
    numberA = randomNumber.nextInt(6) + 1 * numberB;
    question = randomNumber.nextInt(3) + 1;


    if (question == 1) {
      question1 = (numberA / numberB) as int;
      question2 = randomNumber.nextInt(26);
      question3 = randomNumber.nextInt(26);

      // Make sure the number is not duplicate.
      if (question2 == question1 || question2 == question3) {
        question2 = randomNumber.nextInt(26);
      } else if (question3 == question1 || question3 == question2) {
        question3 = randomNumber.nextInt(26);
      }
    }

    if (question == 2) {
      question2 = (numberA / numberB) as int;
      question1 = randomNumber.nextInt(26);
      question3 = randomNumber.nextInt(26);

      if (question1 == question2 || question1 == question3) {
        question1 = randomNumber.nextInt(26);
      } else if (question3 == question2 || question3 == question1) {
        question3 = randomNumber.nextInt(26);
      }
    }

    if (question == 3) {
      question3 = (numberA / numberB) as int;
      question1 = randomNumber.nextInt(26);
      question2 = randomNumber.nextInt(26);

      if (question1 == question3 || question1 == question2) {
        question1 = randomNumber.nextInt(26);
      } else if (question2 == question3 || question2 == question1) {
        question2 = randomNumber.nextInt(26);
      }
    }
  }
}
